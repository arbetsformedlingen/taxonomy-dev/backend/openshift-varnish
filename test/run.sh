#!/bin/bash

# Exit immediately if any command fails
set -e

# Build the Docker image
podman build . -t varnish

export TAXONOMYSERVICE="${TAXONOMYSERVICE:-taxonomy-testing-read.api.jobtechdev.se}"

# Resolve the DNS and save the second IP address to a variable
IP_ADDRESS=$(nslookup $TAXONOMYSERVICE | grep 'Address' | awk 'NR==2 { print $2 }')

# Verify that the IP was successfully resolved
echo "Resolved IP"
echo $IP_ADDRESS

# Run the Docker container with the resolved IP
podman run -d -p 8083:8083 -e TAXONOMYSERVICEPORT=80 -e TAXONOMYSERVICE=$IP_ADDRESS --name varnish --rm varnish

# Wait for Varnish to be ready
echo "Waiting for Varnish to start 10s..."
sleep 10

# Run tests
echo "Run tests"
bash ./test/ci-tests.sh localhost:8083

# Get varnish logs
echo "Varnish logs"
podman logs varnish

# Stop the Varnish container
podman container stop varnish

