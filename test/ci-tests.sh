#!/bin/bash

# Set host to the first argument if provided, otherwise use the default value
host="${1:-https://taxonomy.api.jobtechdev.se}"
echo "Using host: $host"

# Function to generate a random string
generate_random_string() {
  head -c 13 /dev/urandom | base64 | tr -dc 'A-Za-z0-9' | head -c 13
}

# Function to extract the Age header value
extract_age() {
  local response="$1"
  echo "$response" | grep -i "^Age:" | awk '{print $2}' | tr -d '[:space:]'
}

# Generate a random string for the label
random_label=$(generate_random_string)
echo "Generated random label: $random_label"

# Define the request URL
request_url="$host/v1/taxonomy/main/concepts?preferred-label=$random_label"
echo "testing with $request_url"

# First request
echo "First request"
RES1=$(curl -IL -X GET "$request_url" -H  "accept: application/json" -H  "api-key: tests" -H "Host: taxonomy-testing-read.api.jobtechdev.se")
echo "Response 1: $RES1"

# Sleep for 3 seconds
echo "Sleeping for 3 seconds to allow caching age to grow"
sleep 3

# Second request, just repeat the first
echo "Second request"
RES2=$(curl -IL -X GET "$request_url" -H  "accept: application/json" -H  "api-key: tests" -H "Host: taxonomy-testing-read.api.jobtechdev.se")
echo "Response 2: $RES2"

# Extract Age from the first response
age1=$(extract_age "$RES1")
echo "Age from first response: $age1"

# Extract Age from the second response
age2=$(extract_age "$RES2")
echo "Age from second response: $age2"

if [[ "$age1" -eq 0 ]] && (( age2 > age1 )); then
  echo "Success: The Age increased from $age1 to $age2"
  exit 0
else
  echo "Failure: The Age did not increase as expected. Age1: $age1, Age2: $age2"
  exit 1
fi
