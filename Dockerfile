FROM alpine:3.21.3

RUN apk add -q varnish envsubst

# Copy launch and configuration files
COPY varnish.vcl /etc/varnish/default.vcl.tmpl
COPY buckets/buckets.vcl /etc/varnish/buckets.vcl
COPY buckets/storages.txt /etc/varnish/storages.txt

RUN chown -R root /etc/varnish  && \
    chgrp -R root /etc/varnish && \
    chmod a+rw /etc/varnish/default.vcl.tmpl && \
    touch /etc/varnish/default.vcl && \
    chmod a+w /etc/varnish/default.vcl && \
    chmod a+r /etc/varnish/storages.txt && \
    chmod a+rwx /etc/varnish && \
    chmod -R a+rwx /etc/varnish
COPY launch-varnish.sh /usr/local/bin/launch-varnish.sh
RUN chmod a+rx /usr/local/bin/launch-varnish.sh && \
    mkdir /var/lib/varnish/varnishd && \
    chmod a+rwx /var/lib/varnish && \
    chmod a+rwx /var/lib/varnish/varnishd

# Default start command
CMD /usr/local/bin/launch-varnish.sh
