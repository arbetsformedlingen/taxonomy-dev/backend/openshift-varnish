vcl 4.0;
import std;
import directors;

# ACL for purging
acl purge {
  "localhost";
}

backend default {
  .host = "${TAXONOMYSERVICE}";
  .port = "${TAXONOMYSERVICEPORT}";
  .first_byte_timeout = 600s;
  .between_bytes_timeout = 600s;
}

sub vcl_init {
    new bar = directors.round_robin();
    bar.add_backend(default);
}

sub vcl_recv {
  # Rewrite redirects
  set req.http.port = "${TAXONOMYSERVICEPORT}";
  set req.backend_hint = bar.backend();

  # Ignore cookies since they are not relevant in the Taxonomy
  # save the cookies before the built-in vcl_recv
  if (req.http.Cookie) {
    set req.http.Cookie-Backup = req.http.Cookie;
    unset req.http.Cookie;
  }

  # Do not store requests to private and status endpoints in the cache
  if ((req.url ~ "/private/") ||
      (req.url ~ "/status/")) {
    return (pass);
  }

  # Cache POST requests
  if (req.method == "POST" && req.url ~ "/v1/taxonomy/graphql") {
    std.log("Will cache POST for: " + req.http.host + req.url);
    std.cache_req_body(1KB);
    return (hash);
  }

  # Check if this is a purge request and that it is allowed
  if (req.method == "PURGE") {
    if (!client.ip ~ purge) {
      return(synth(405,"Not allowed."));
    }
    return (purge);
  }
}

sub vcl_hash {
  if (req.http.Cookie-Backup) {
    # restore the cookies before the lookup if any
    set req.http.Cookie = req.http.Cookie-Backup;
    unset req.http.Cookie-Backup;
  }
}

sub vcl_backend_response {
  # set TTLs and Grace
  set beresp.ttl = 1w;
  set beresp.grace = 2h;
  set beresp.do_stream = false;

  # do not cache anything other than 200s
  if (beresp.status != 200) {
    set beresp.ttl = 0s;
    set beresp.grace = 0s;
  }

  include "buckets.vcl";

  # Tells the client which backend varnish communicated with
  set beresp.http.x-backend = beresp.backend.name;

  # Remove Cache-Control and Set-Cookie from response
  unset beresp.http.Cache-Control;
  unset beresp.http.Set-Cookie;
}

sub vcl_backend_error {
  return (retry);
}

sub vcl_deliver {
  unset resp.http.x-backend;
  set resp.http.Access-Control-Allow-Origin = "*";
}

