(ns vclgen.test
  (:require [clj-http.client :as client]))

(def default-config {:addresses ["http://localhost:8083"]
                     :connection-timeout 20000})


(defn disp-result [result]
  (def res result)
  (let [{:keys [key index status address message headers]} result]
    (println (format "Performed query %s index %d with status %s to %s\n  Bucket: %s"
                     (str key)
                     index
                     (str status)
                     address
                     (get headers "x-storage")))
    (when message
      (println "  Message:" message))
    result))

(defn run-queries [config queries]
  {:pre [(sequential? queries)
         (every? :string queries)
         (every? :key queries)]}
  (let [{:keys [addresses connection-timeout]} config
        timestamp (System/currentTimeMillis)]
    
    (vec (for [[i [base-address query]] (map-indexed vector (for [address addresses
                                                                  query queries]
                                                              [address query]))
               :let [query-string (format (:string query) timestamp i)]]
           (let [address (str base-address "/v1/taxonomy/graphql")]
             (disp-result
              (merge query
                     config
                     {:index i
                      :address address
                      :query query-string}
                     (try
                       (client/get
                        address
                        {:accept :json
                         :query-params {"query" query-string}
                         :connection-timeout connection-timeout})
                       (catch Exception e
                         {:exception e
                          :message (str e)
                          :status :exception})))))))))

(def q-small "query DebugGraphQL_%014d_%02d {
  concepts(type: \"skill\", version: \"1\") {
    id
    preferred_label
    type
    deprecated_legacy_id
    broader(type: \"skill-headline\") {
      id
      preferred_label
    }
  }
}
")


(def q-large "query DebugGraphQL_Large_%014d_%02d {   concepts {alternative_labels esco_uri definition eures_code_2014 eures_nace_code_2007 hidden_labels id preferred_label short_description ssyk_code_2012 type uri broad_match {id} broader {id} close_match {id} exact_match {id} narrow_match {id} narrower {id} possible_combinations {id} related {id} replaced_by {id} replaces {id} substituted_by {id} substitutes {id} unlikely_combinations {id}}}
")

(defmacro make-query [varname]
  `{:string ~varname
    :key ~(keyword varname)})

(def queries {:small (make-query q-small)
              :large (make-query q-large)})

(defn test-api [{:keys [address query-seq]}]
  (run-queries (assoc default-config :addresses [address])
               (into []
                     cat
                     (for [[query-key n] query-seq]
                       (repeat n (get queries query-key))))))


(comment
  (do

    (test-api {:address "http://localhost:8083"
               :query-seq [[:small 10] [:large 10]]})

    ))
