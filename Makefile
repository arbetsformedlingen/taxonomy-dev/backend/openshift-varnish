.PHONY: docker-image docker-run docker-stop generate-buckets test-varnish-local test-varnish-prod

docker-image:
	podman build -t varnish-demo .

# Before calling `docker-run`, you need to start a local Taxonomy API server.
docker-run:
	podman run --net=host -p 8083:8083 -e TAXONOMYSERVICEPORT=8080 -e TAXONOMYSERVICE=localhost --name varnish-demo --rm varnish-demo

docker-stop:
	podman container stop varnish-demo


## Testing

test-varnish-local:
	clj -X:task test-api :address '"http://localhost:8083"' :query-seq '[[:small 10] [:large 10]]'

test-varnish-prod:
	clj -X:task test-api :address '"https://taxonomy.api.jobtechdev.se"' :query-seq '[[:small 10] [:large 10]]'

# Bucket generation: Only  call this if you update buckets_config.edn
generate-buckets:
	clj -X:task produce :buckets-config-file '"buckets_config.edn"' :output-root '"buckets"'
