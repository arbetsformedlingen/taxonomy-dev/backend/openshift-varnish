#!/bin/sh

if [ -z "$TAXONOMYSERVICEPORT" ] || [ -z "$TAXONOMYSERVICE" ]
then
  echo "Environment variables TAXONOMYSERVICE and TAXONOMYSERVICEPORT needs to be set. See README.md"
  exit -1
else
  echo "Using TAXONOMYSERVICE=$TAXONOMYSERVICE and TAXONOMYSERVICEPORT=$TAXONOMYSERVICEPORT"
fi

# Generate varnish config from env variables
envsubst < /etc/varnish/default.vcl.tmpl > /etc/varnish/default.vcl

#
# ***IMPORTANT NOTE***
#
# If you edit the `nuke_limit` here, you may have to update `:span-factor`
# in `buckets_config.edn` and call `make generate-buckets`.
#
varnishd $(cat /etc/varnish/storages.txt) -p nuke_limit=50 -p vsl_reclen=2048 -a :8083 -p vcl_path=/etc/varnish -f default.vcl

varnishncsa -F '%{Host}i %h %l %u %t %{Varnish:handling}x %s %D %b "%r" "%{Referer}i" "%{User-agent}i" "%{api-key}i"'
