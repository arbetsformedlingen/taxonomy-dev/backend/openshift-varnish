  # This code is generated.
  # The code classifies requests into buckets of requests with about the same size.
  
  # 6 buckets with a total storage of 2893 MB:
  #  * bucket0: [0, 30[ , capacity=50 MB
  #  * bucket1: [30, 900[ , capacity=108 MB
  #  * bucket2: [900, 27000[ , capacity=233 MB
  #  * bucket3: [27000, 810000[ , capacity=501 MB
  #  * bucket4: [810000, 24300000[ , capacity=1000 MB
  #  * bucket5: [24300000, 104857600[ , capacity=1000 MB
  
  if (std.integer(beresp.http.Content-Length, 0) < 30) {
    set beresp.storage = storage.bucket0;
    set beresp.http.x-storage = "bucket0";
  } elsif (std.integer(beresp.http.Content-Length, 0) < 900) {
    set beresp.storage = storage.bucket1;
    set beresp.http.x-storage = "bucket1";
  } elsif (std.integer(beresp.http.Content-Length, 0) < 27000) {
    set beresp.storage = storage.bucket2;
    set beresp.http.x-storage = "bucket2";
  } elsif (std.integer(beresp.http.Content-Length, 0) < 810000) {
    set beresp.storage = storage.bucket3;
    set beresp.http.x-storage = "bucket3";
  } elsif (std.integer(beresp.http.Content-Length, 0) < 24300000) {
    set beresp.storage = storage.bucket4;
    set beresp.http.x-storage = "bucket4";
  } elsif (std.integer(beresp.http.Content-Length, 0) < 104857600) {
    set beresp.storage = storage.bucket5;
    set beresp.http.x-storage = "bucket5";
  } else {
    set beresp.ttl = 0s;
    set beresp.grace = 0s;
    set beresp.uncacheable = true;
    return (deliver);
  }